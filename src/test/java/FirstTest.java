import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Optional;

public class FirstTest {
    @Test
    public void checkGoogleIsOpened(){
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.get("http://www.google.com");
       WebElement element = driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input"));
       boolean result = element.isEnabled();
      if (!result){
          driver.quit();
        Assert.assertTrue(result, "Element is Enable");

      }
       driver.quit();

    }
    @Test
    public void checkElementOnPage(){

    }
}
