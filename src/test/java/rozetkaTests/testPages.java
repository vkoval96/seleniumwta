package rozetkaTests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class testPages {
    @Test
    public void menuLInksAreAllowed() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        //driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        driver.manage().window().maximize();
        WebElement firstLink = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-main-page/div/aside/main-page-sidebar/sidebar-fat-menu/div/ul/li[1]/a"));
        firstLink.click();
        WebElement elem = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/app-root/div/div/rz-super-portal/div/main/section/div[2]/rz-dynamic-widgets/rz-widget-list[1]/section/ul/li[2]/rz-list-tile/div")));
        driver.quit();
    }

    @Test
    public void iconMyAccountShowRegistrPage() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        driver.manage().window().maximize();
        WebElement firstLink = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-header/rz-main-header/header/div/div/ul/li[3]/rz-user/button"));
        firstLink.click();
        WebElement emailField = driver.findElement(By.xpath("//*[@id=\"auth_email\"]"));
        WebElement passwordField = driver.findElement(By.xpath("//*[@id=\"auth_pass\"]"));
        emailField.click();
        passwordField.click();
        boolean result = emailField.isEnabled() & passwordField.isEnabled();
        Assert.assertTrue(result, "Fields should be enable");
        driver.quit();
    }

    @Test
    public void registrationProcess() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        driver.manage().window().maximize();
        WebElement firstLink = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-header/rz-main-header/header/div/div/ul/li[3]/rz-user/button"));
        firstLink.click();
        WebElement emailField = driver.findElement(By.xpath("//*[@id=\"auth_email\"]"));
        emailField.click();
        emailField.sendKeys("vkoval96@gmail.com");
        WebElement passwordField = driver.findElement(By.xpath("//*[@id=\"auth_pass\"]"));
        passwordField.click();
        passwordField.sendKeys("5500Satio");
        WebElement registerButton = driver.findElement(By.xpath("/html/body/app-root/single-modal-window/div[3]/div[2]/rz-user-identification/rz-auth/div/form/fieldset/div[5]/button[1]"));
        registerButton.click();
        WebElement imNotRobotButton = driver.findElement(By.xpath("//*[@id=\"recaptcha-anchor\"]/div[1]"));
        imNotRobotButton.click();
        registerButton.click();
        WebElement phoneCode = driver.findElement(By.xpath("//*[@id=['confirmPhoneCode']"));
        phoneCode.click();
        phoneCode.sendKeys("244930");
        WebElement confirm = driver.findElement(By.xpath("/html/body/app-root/single-modal-window/div[3]/div[2]/rz-user-identification/rz-confirm-phone/div/form/fieldset/div[3]/button"));
        confirm.click();
        driver.quit();
    }

    @Test
    public void macbookSearching() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        driver.manage().window().maximize();
        WebElement searcher = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-header/rz-main-header/header/div/div/div/form/div/div[1]/input"));
        searcher.sendKeys("Macbook" + Keys.ENTER);
        WebElement Pro13 = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-category/div/main/rz-catalog/div/div/section/rz-grid/ul/li[2]/rz-catalog-tile/app-goods-tile-default/div/div/a[@title = 'Ноутбук Apple MacBook Pro 16\" M1 Pro 512GB 2021 (Z14V000RA) Space Gray']"));
        Pro13.click();
        WebElement buyItem = driver.findElement(By.xpath("//*[@id=\"#scrollArea\"]/div[1]/div[2]/rz-product-main-info/div[2]/div/ul/li[1]/app-product-buy-btn/app-buy-button/button"));
        boolean result = buyItem.isEnabled();
        Assert.assertTrue(result, "Item should be enable");
        driver.quit();
    }

    @Test
    public void TechnicCategories() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        driver.manage().window().maximize();
        WebElement menu = driver.findElement(By.xpath("//*[@id=\"fat-menu\"]"));
        menu.click();
        WebElement CatFood = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-header/rz-main-header/header/div/div/rz-header-fat-menu/fat-menu/div/ul/li[13]/a"));
        CatFood.findElement(By.xpath("/html/body/app-root/div/div/rz-header/rz-main-header/header/div/div/rz-header-fat-menu/fat-menu/div/ul/li[13]/div/div[2]/div[1]/div[1]/ul[1]/li/ul/li[1]/a"))
                .click();
        WebElement Prurina = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-category/div/main/rz-catalog/div/div/section/rz-grid/ul/li[2]/rz-catalog-tile/app-goods-tile-default/div/div[2]/span"));
        boolean result = Prurina.isDisplayed();
        Assert.assertTrue(result, "Item should be displayed");
        driver.quit();
    }

    @Test
    public void KitchenTechnic() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        driver.manage().window().maximize();
        WebElement menu = driver.findElement(By.xpath("//*[@id=\"fat-menu\"]"));
        menu.click();
        WebElement technic = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-header/rz-main-header/header/div/div/rz-header-fat-menu/fat-menu/div/ul/li[4]/a"));
        technic.click();
        WebElement list = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-super-portal/div/main/section/div[2]/rz-dynamic-widgets/rz-widget-list[1]/section/ul/li[2]/rz-list-tile/div//*[@title = 'Встраиваемая техника']"));
        list.click();
        boolean result = list.isEnabled();
        Assert.assertTrue(result, "Item should be enable");
        driver.quit();
    }

    @Test
    public void AdditionalInformation() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        driver.manage().window().maximize();
        WebElement menu = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-header/rz-main-header/header/div/div/rz-mobile-user-menu/button"));
        menu.click();
        WebElement help = driver.findElement(By.xpath("//*[@id=\"cdk-overlay-0\"]/nav/div/div[2]/ul[2]/li[1]/a[1]"));
        help.click();
        boolean result = help.isEnabled();
        Assert.assertTrue(result, "Item should be enable");
        driver.quit();
    }

    @Test
    public void AboutInformation() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        driver.manage().window().maximize();
        WebElement menu = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-header/rz-main-header/header/div/div/rz-mobile-user-menu/button"));
        menu.click();
        WebElement informe = driver.findElement(By.xpath("//*[@id=\"cdk-overlay-0\"]/nav/div/div[2]/ul[2]/li[3]/rz-service-links/div[1]/ul/li[1]/a"));
        informe.click();
        boolean result = informe.isEnabled();
        Assert.assertTrue(result, "Item should be enable");
        driver.quit();
    }

    @Test
    public void GooglePlayRef() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        driver.manage().window().maximize();
        WebElement googlePlay = driver.findElement(By.xpath("//a[@title = 'Приложение для Андроида']"));
        googlePlay.click();
        driver.get("https://play.google.com/store/apps/details/?id=ua.com.rozetka.shop&referrer=utm_source%3Dfullversion%26utm_medium%3Dsite%26utm_campaign%3Dfullversion");
        WebElement googleNewTab = driver.findElement(By.xpath("//*[@id=\"fcxH9b\"]/div[4]/c-wiz/div/div[2]/div/div/main/c-wiz[1]/c-wiz[1]/div/div[2]/div/div[2]/div/div[2]/div/c-wiz/c-wiz/div/span/button"));
        googleNewTab.click();
        boolean result = googleNewTab.isEnabled();
        Assert.assertTrue(result, "Link should be enable");
        driver.quit();
    }

    @Test
    public void AppStoreRef() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        driver.manage().window().maximize();
        WebElement applePlay = driver.findElement(By.xpath("//a[@title = 'Приложение для Айфона']"));
        applePlay.click();
        driver.get("https://apps.apple.com/app/apple-store/id740469631");
        WebElement appleNewTab = driver.findElement(By.xpath("//*[text() = 'ROZETKA - интернет-покупки']"));
        boolean result = appleNewTab.isDisplayed();
        Assert.assertTrue(result, "Link should be enable");
        driver.quit();
    }

    @Test
    public void SocialNetworkFacebook() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        driver.manage().window().maximize();
        WebElement facebookLink = driver.findElement(By.xpath("//*[@aria-label = 'Facebook']"));
        facebookLink.click();
        driver.get("https://www.facebook.com/rozetka.ua");
        WebElement facebookNewTab = driver.findElement(By.xpath("//*[@aria-label = 'Not Logged In']"));
        boolean result = facebookNewTab.isDisplayed();
        Assert.assertTrue(result, "Link should be enable");
        driver.quit();
    }

    @Test
    public void SocialNetworkTwitter() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        driver.manage().window().maximize();
        WebElement twitterLink = driver.findElement(By.xpath("//*[@aria-label = 'Twitter']"));
        twitterLink.click();
        driver.get("https://twitter.com/rozetka_ua");
        WebElement twitterNewTab = driver.findElement(By.xpath("//*[@aria-label = 'Follow @rozetka_ua']"));
        boolean result = twitterNewTab.isEnabled();
        Assert.assertTrue(result, "Link should be enable");
        driver.quit();
    }

    @Test
    public void SocialNetworkYouTube() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        driver.manage().window().maximize();
        WebElement youtubeLink = driver.findElement(By.xpath("//*[@aria-label = 'YouTube']"));
        youtubeLink.click();
        driver.get("https://www.youtube.com/channel/UCr7r1-z79TYfqS2IPeRR47A");
        WebElement youtubeNewTabSub = driver.findElement(By.xpath("//*[@aria - label ='Subscribe'"));
        WebElement youtubeNewTabDec = driver.findElement(By.xpath("//*[@aria - label ='Cancel'"));
        boolean result = youtubeNewTabDec.isEnabled() & youtubeNewTabSub.isEnabled();
        Assert.assertTrue(result, "Link should be enable");
        //driver.quit();
    }

    @Test
    public void SocialNetworkInst() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        driver.manage().window().maximize();
        WebElement instLink = driver.findElement(By.xpath("//*[@aria-label = 'Instagram']"));
        instLink.click();
        driver.get("https://www.instagram.com/rozetkaua/");
        WebElement instSub = driver.findElement(By.xpath("//*[@id=\"react-root\"]/section/main/div/header/section/div[1]/div[2]/div/div/a/button"));
        boolean result = instSub.isEnabled();
        Assert.assertTrue(result, "Link should be enable");
        //driver.quit();
    }

    @Test
    public void SocialNetworkViber() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        driver.manage().window().maximize();
        WebElement viberLink = driver.findElement(By.xpath("//*[@aria-label = 'Viber']"));
        viberLink.click();
        driver.get("https://invite.viber.com/?g2=AQB9mwM%2F5f%2FxJUlMxP4V9flr2%2BvXTC1MpxdGFZ0P6d%2Fs6Ws%2FFe%2FQtLiZwA4E28sj&lang=en");
        WebElement viberQR = driver.findElement(By.xpath("/html/body/app-root/vbr-page/vbr-content/app-main/app-account/article/section[2]/div[1]/div[1]/div[3]/app-qr-code/div/div/canvas"));
        boolean result = viberQR.isDisplayed();
        Assert.assertTrue(result, "Link should be displayed");
        //driver.quit();
    }

    @Test
    public void SocialNetworkTelegram() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        driver.manage().window().maximize();
        WebElement telegramLink = driver.findElement(By.xpath("//*[@aria-label = 'Telegram']"));
        telegramLink.click();
        driver.get("https://t.me/rrozetka");
        WebElement tgLnk = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[5]/a"));
        boolean result = tgLnk.isEnabled();
        Assert.assertTrue(result, "Link should be enable");
        //driver.quit();
    }

    @Test
    public void LinkForSaleByPrice() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        driver.manage().window().maximize();
        WebElement forsaleLink = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-main-page/div/aside/main-page-sidebar/sidebar-fat-menu/div/ul/li[18]/a"));
        forsaleLink.click();
        forsaleLink = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-promotions/div/main/rz-catalog/div/div/section/rz-grid/ul/li[5]/rz-promotion-tile/a/span/span/span"));
        forsaleLink.click();
        WebElement regLink = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-promotion/div/div/rz-promotion-banner/section/div/div/div[2]/a"));
        boolean result = regLink.isEnabled();
        Assert.assertTrue(result, "Link should be enable");
        //driver.quit();
    }

    @Test
    public void LinkForSale() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        driver.manage().window().maximize();
        WebElement forsaleLink = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-main-page/div/aside/main-page-sidebar/sidebar-fat-menu/div/ul/li[18]/a"));
        forsaleLink.click();
        forsaleLink = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-promotions/div/main/rz-catalog/div/div/section/rz-grid/ul/li[5]/rz-promotion-tile/a/span/span/span"));
        forsaleLink.click();
        WebElement regLink = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-promotion/div/div/rz-promotion-banner/section/div/div/div[2]/a"));
        WebElement minPrice = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-promotion/div/main/rz-catalog/div/div/aside/rz-filter-stack/div[3]/div/rz-scrollbar/div/div[1]/div/div/rz-filter-slider/form/fieldset/div/input[1]"));
        minPrice.sendKeys("100");
        WebElement maxPrice = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-promotion/div/main/rz-catalog/div/div/aside/rz-filter-stack/div[3]/div/rz-scrollbar/div/div[1]/div/div/rz-filter-slider/form/fieldset/div/input[1]"));
        maxPrice.clear();
        maxPrice.sendKeys("100000");
        WebElement second = driver.findElement(By.xpath("//*[text() = ' Ноутбук ASUS ROG Strix SCAR 15 G533ZX-LN060W (90NR08E2-M00430) Black ']"));
        boolean result = second.isEnabled();
        Assert.assertTrue(result, "Link should be enable");
        //driver.quit();
    }

    @Test
    public void Wishlist() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        driver.manage().window().maximize();
        WebElement itemLink = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-main-page/div/aside/main-page-sidebar/sidebar-fat-menu/div/ul/li[4]/a"));
        itemLink.click();
        itemLink = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-super-portal/div/main/section/div[2]/rz-dynamic-widgets/rz-widget-list[2]/section/ul/li[4]/rz-list-tile/div/ul/li[2]/a"));
        itemLink.click();
        WebElement itemList = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-category/div/main/rz-catalog/div/div/section/rz-grid/ul/li[1]/rz-catalog-tile/app-goods-tile-default/div/div[2]/a[2]/span"));
        itemList.click();
        WebElement wishList = driver.findElement(By.xpath("//*[@id=\"#scrollArea\"]/div[1]/div[2]/rz-product-main-info/div[2]/div/ul/li[3]/ul/li[2]/app-goods-wishlist/button"));
        wishList.click();
        boolean result = wishList.isEnabled();
        Assert.assertTrue(result, "Item should be enable");
        //driver.quit();
    }

    @Test
    public void CommentAction() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        driver.manage().window().maximize();
        WebElement itemLink = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-main-page/div/aside/main-page-sidebar/sidebar-fat-menu/div/ul/li[4]/a"));
        itemLink.click();
        itemLink = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-super-portal/div/main/section/div[2]/rz-dynamic-widgets/rz-widget-list[2]/section/ul/li[4]/rz-list-tile/div/ul/li[2]/a"));
        itemLink.click();
        WebElement itemList = driver.findElement(By.xpath("/html/body/app-root/div/div/rz-category/div/main/rz-catalog/div/div/section/rz-grid/ul/li[1]/rz-catalog-tile/app-goods-tile-default/div/div[2]/a[2]/span"));
        itemList.click();
        WebElement commentButton = driver.findElement(By.xpath("//*[@id=\"#scrollArea\"]/div[1]/div[2]/rz-product-main-info/div[2]/div/ul/li[2]/app-product-credit/button"));
        commentButton.click();
        WebElement creditWindow = driver.findElement(By.xpath("/html/body/app-root/single-modal-window/div[3]/div[2]"));
        boolean result = creditWindow.isDisplayed();
        Assert.assertTrue(result, "Button should be displayed");
        driver.quit();
    }
}
